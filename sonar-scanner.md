# SonarQube Scanner


## Scanner installeren
* Wanneer er geen gebruik gemaakt wordt van Maven (voor het Java project) moet de scanner handmatig worden gedownload
* Om de code te analyseren dien je gebruik te maken van de SonarQube Scanner. Download de meest recente versie: https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/ Op het moment van schrijven is dit versie 4.5.0
* Pak deze uit in de Sonarqube directory, zodat dit er als volgt 'uitziet':

````
C:\sonarqube\sonar-scanner-4.5.0
````
of
````
/etc/sonarqube/sonar-scanner-4.5.0
````

## Project configureren
* Om het project te configureren zodat dit door de scanner wordt gehaald, dien je in de root van je project (repository) een property bestand aan te maken:  **sonar-project.properties** met de volgende [inhoud.](https://gitlab.com/dbouman/sonarqube/raw/master/sonar-project.properties "Klik hier")
* Verander in dit bestand 2x!! **NAAM_VAN_JE_PROJECT** naar de naam van je eigen Project. Save daarna het bestand!

## Project analyseren
* Open een prompt/terminal **op de locatie van je project**
* Type hier het volledige path naar de sonar-scanner.bat/sh-file:

````
C:\sonarqube\sonar-scanner-4.5.0\bin\sonar-scanner.bat
````
of
````
/etc/sonarqube/sonar-scanner-4.5.0/bin/sonar-scanner
````

* Druk op Enter
* Wacht tot er **EXECUTION SUCCESS** staat